#include <stdio.h>
#include <time.h>

int main(){
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // body
    int b;
    int a;
    fscanf(stdin,"%d\n%d",&a,&b);
    a=a^b;
    b=a^b;
    a=a^b;
    fprintf(stdout,"%d\n%d\n\n",a,b);
    // end body
    end = clock();
    cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
    fprintf(stdout,"%.20f",cpu_time_used);
}
#include <stdio.h>
#include <time.h>

int main(){
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // body
    int k;
    int n;
    fscanf(stdin,"%d %d",&n,&k);
    int i=1;
    int kol=0;
    n-=1;
    while(n>=k){
        n-=i;
        if (i<k){
            i++;
        }
        kol++;
    }
    if (n>0){
        kol++;
    }
    fprintf(stdout,"%d\n\n",kol);
    // end body
    end = clock();
    cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
    fprintf(stdout,"%.20f",cpu_time_used);
}
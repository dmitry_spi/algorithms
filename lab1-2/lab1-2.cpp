#include <stdio.h>
#include <time.h>

int Factorial(int num){
    if (num==0){
        return 1;
    }
    return num * Factorial(num-1);
}
// not working. need fix
int main(){
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // body
    int sum = 0;
    for (int i=0; i<=13; i++){
        if (i<10){
            sum+=Factorial(i+2)/(2*Factorial(i));
        }
        else{
            sum+=(Factorial(i+2)/(2*Factorial(i)))-3*(Factorial(i+2-10)/2*Factorial(i-10));
        }
    }
    sum*=2;
    fprintf(stdout,"%d\n\n",sum);
    // end body
    end = clock();
    cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
    fprintf(stdout,"%.20f",cpu_time_used);
}
#include <stdio.h>
#include <time.h>
#include <stack>
#include <string>

int main(){
    using namespace std;
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // body
    stack <char> s;
    char str[100];
    fscanf(stdin,"%s",str);
    int result = 1;
    for (int i=0; i<100; i++){
        if (str[i]=='{' || str[i]=='[' || str[i]=='('){
            s.push(str[i]);
        }
        if (str[i]=='}'){
            char& c = s.top();
            if (c!='{'){
                result = 0;
                break;
            }
            s.pop();
        }
        if (str[i]==']'){
            char& c = s.top();
            if (c!='['){
                result = 0;
                break;
            }
            s.pop();
        }
        if (str[i]==')'){
            char& c = s.top();
            if (c!='('){
                result = 0;
                break;
            }
            s.pop();
        }
    }
    fprintf(stdout,"%d\n\n",result);
    // end body
    end = clock();
    cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
    fprintf(stdout,"%.20f",cpu_time_used);
}
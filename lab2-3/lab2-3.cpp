#include <stdio.h>
#include <time.h>
#include <math.h>

int main(){
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // body
    int n;
    int m;
    int result;
    fscanf(stdin,"%d %d",&n, &m);
    if (m<n){
        result = m+1;
    }
    else{
        result = (m%n)+1;
    }
    fprintf(stdout,"%d\n\n",result);
    // end body
    end = clock();
    cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
    fprintf(stdout,"%.20f",cpu_time_used);
}
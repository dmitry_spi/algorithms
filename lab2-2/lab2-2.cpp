#include <stdio.h>
#include <time.h>
#include <stack>
#include <string>

int main(){
    using namespace std;
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // body
    stack <char> s;
    char str[100];
    fscanf(stdin,"%s",str);
    float result = 0;
    for (int i=0; i<100; i++){
        if (str[i]=='{' || str[i]=='[' || str[i]=='('){
            s.push(str[i]);
        }
        if (str[i]=='}'){
            char& c = s.top();
            if (c!='{'){
                result = -1;
                break;
            }
            s.pop();
        }
        if (str[i]==']'){
            char& c = s.top();
            if (c!='['){
                result = -1;
                break;
            }
            s.pop();
        }
        if (str[i]==')'){
            char& c = s.top();
            if (c!='('){
                result = -1;
                break;
            }
            s.pop();
        }
    }
    if (result==-1){
        fprintf(stdout,"%s\n\n","no");
    }
    stack <float> s1;
    stack <char> s2;
    for (int i=99; i>=0; i--){
        if (str[i]=='{' || str[i]=='[' || str[i]=='('){
            continue;
        }
    }
    fprintf(stdout,"%.2f\n\n",result);
    // end body
    end = clock();
    cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
    fprintf(stdout,"%.20f",cpu_time_used);
}
#include <stdio.h>
#include <time.h>
#include <math.h>

int main(){
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // body
    int k;
    int r;
    fscanf(stdin,"%d %d\n",&k,&r);
    int (*x)[2] = new int[k][2];
    for(int i=0; i<k; i++) {
        fscanf(stdin,"%d %d\n",&x[i][0],&x[i][1]);
    }
    float sum;
    for (int i=0; i<k; i++){
        if (i==k-1){
            sum+=sqrt(abs(x[i][0]-x[0][0])+abs(x[i][1]-x[0][1]));
        }
        else{
            sum+=sqrt(abs(x[i][0]-x[i+1][0])+abs(x[i][1]-x[i+1][1]));
        }
    }
    sum+=2*3.1415*r;
    fprintf(stdout,"%.2f\n\n",sum);
    // end body
    end = clock();
    cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
    fprintf(stdout,"%.20f",cpu_time_used);
}